#ifndef SHOPPINGLIST_H
#define SHOPPINGLIST_H

#include <QDialog>
#include <QStringListModel>

namespace Ui {
class ShoppingList;
}

class ShoppingList : public QDialog
{
    Q_OBJECT

public:
    explicit ShoppingList(QWidget *parent = nullptr);
    ~ShoppingList();

private slots:
    void on_pushButtonCalculate_clicked();

    void on_pushButtonExit_clicked();

private:
    Ui::ShoppingList *ui;
    QStringListModel *listViewModel;
};

#endif // SHOPPINGLIST_H
