#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "entrydialog.h"
#include "recipe.h"
#include <QStringListModel>
#include <string>
#include "shoppinglist.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void contentSetup();
    void deleteRecipe(QString& oldKey);

public slots:
    void updateSetup()
    {
        contentSetup();
    }

private slots:
    void on_pushButtonMainAdd_clicked();
    void on_pushButtonEdit_clicked();
    void on_pushButtonDelete_clicked();

    void on_actionExit_triggered();

    void on_actionCreate_Shopping_List_triggered();

private:
    Ui::MainWindow *ui;
    EntryDialog *entryDialog;
    QStringListModel *listViewModel;
    ShoppingList* shoppingDialog;
};
#endif // MAINWINDOW_H
