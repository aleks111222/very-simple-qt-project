#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "json.hpp"
#include <fstream>
#include <QDebug>
#include <QStringListModel>

using json = nlohmann::json;

void MainWindow::deleteRecipe(QString& oldKey) {

    std::ifstream file("recipes.json");
    json jf = json::parse(file);

        for (json::iterator it = jf.begin(); it != jf.end(); ++it)
        {
           std::string name = it.key();

           if (name == oldKey.toStdString() && jf.size() != 1)
           {
               jf.erase(it++);
           } else if (name == oldKey.toStdString()) {
               jf.erase(it);
               break;
           }
        }

    file.close();

    std::ofstream File("recipes.json");

    File << jf;

    File.close();

    contentSetup();
}

void MainWindow::contentSetup() {

    QStringList RecipeList;

    std::ifstream file("recipes.json");
    if (!file) {
           std::fstream newFile;
           newFile.open("recipes.json",std::ios::out);
           std::string emptyFile = "{}";
           newFile << emptyFile;
           newFile.close();
           return;
    }
    json jf = json::parse(file);

    std::string Name;

    for (json::iterator it = jf.begin(); it != jf.end(); ++it) {
         Name = it.key();
         QString qName = QString::fromStdString(Name);
         RecipeList << qName;
    }

    file.close();

    listViewModel->setStringList(RecipeList);
    ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listView->setModel(listViewModel);
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    listViewModel = new QStringListModel(this);

    contentSetup();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButtonMainAdd_clicked()
{
    entryDialog = new EntryDialog(this);
    connect(entryDialog,SIGNAL(JSONchanged()),this, SLOT(updateSetup()));
    entryDialog->show();
}

void MainWindow::on_pushButtonEdit_clicked()
{
    QModelIndex index = ui->listView->currentIndex();
    QString oldKey = index.data(Qt::DisplayRole).toString();
    entryDialog = new EntryDialog(oldKey, this);
    connect(entryDialog,SIGNAL(JSONchanged()),this, SLOT(updateSetup()));
    entryDialog->show();
}

void MainWindow::on_pushButtonDelete_clicked()
{
    QModelIndex index = ui->listView->currentIndex();
    QString oldKey = index.data(Qt::DisplayRole).toString();
    deleteRecipe(oldKey);
}

void MainWindow::on_actionExit_triggered()
{
    exit(0);
}

void MainWindow::on_actionCreate_Shopping_List_triggered()
{
    shoppingDialog = new ShoppingList(this);
    shoppingDialog->show();
}
