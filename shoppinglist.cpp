#include "shoppinglist.h"
#include "ui_shoppinglist.h"
#include <string>
#include <sstream>
#include "json.hpp"
#include <fstream>

using json = nlohmann::json;

ShoppingList::ShoppingList(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ShoppingList)
{
    ui->setupUi(this);
    listViewModel = new QStringListModel(this);
}

ShoppingList::~ShoppingList()
{
    delete ui;
}

void ShoppingList::on_pushButtonCalculate_clicked()
{
    std::string names[100];
    float values[100];
    std::string units[100];

    for (int i = 0; i < 100; i++) {
        names[i] = "";
        values[i] = 0;
        units[i] = "";
    }

    QStringList qRecipes = ui->textEditRecipes->toPlainText().split('\n');

    std::string recipes[qRecipes.size()];

    for (int i = 0; i < qRecipes.size(); i ++) {

        recipes[i] = qRecipes[i].toStdString();
    }

    std::ifstream file("recipes.json");
    json jf = json::parse(file);

        for (int i = 0; i < (*(&recipes + 1) - recipes); i++) {

            for (json::iterator it = jf[recipes[i]].begin(); it != jf[recipes[i]].end(); ++it)
            {
                if (it.key() != "recipe")
                {
                    int ok = 0;

                    std::string name = it.key();
                    std::string value = it.value();

                    float quantity = std::stof(value.substr(0, value.find(" ")));
                    std::string unit = value.substr(value.find(" ") + 1, value.length() - 1);

                    for (int j = 0; j < 100; j++) {

                        if (names[j] == name && units[j] == unit) {
                            values[j] += quantity;
                            ok = 1;
                            break;
                        }
                    }
                    if (ok == 1) {
                        continue;
                    }

                    for (int j = 0; j < 100; j++) {

                        if (names[j] == "") {
                            names[j] = name;
                            values[j] = quantity;
                            units[j] = unit;
                            break;
                        }
                    }
                }
            }
        }

    QStringList shoppingList;

    for (int i = 0; i < (*(&names + 1) - names); i++) {

        QString newEntry;

        if (names[i] != "") {

            std::stringstream ss;
            ss << values[i];

            newEntry.append(QString::fromStdString(names[i]) + " " +
                            QString::fromStdString(ss.str()) + " " +
                            QString::fromStdString(units[i]));
            shoppingList << newEntry;
        }
    }

    listViewModel->setStringList(shoppingList);
    ui->listViewShoppingList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listViewShoppingList->setModel(listViewModel);
}

void ShoppingList::on_pushButtonExit_clicked()
{
    this->close();
}
