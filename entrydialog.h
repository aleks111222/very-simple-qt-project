#ifndef ENTRYDIALOG_H
#define ENTRYDIALOG_H

#include <QDialog>
#include <QAbstractButton>
#include "json.hpp"
#include <QStringListModel>

using json = nlohmann::json;

namespace Ui {
class EntryDialog;
}

class EntryDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EntryDialog(QWidget *parent = nullptr);
    explicit EntryDialog(QString oldKey = "", QWidget *parent = nullptr);

    ~EntryDialog();
    void updateJSON(json&);
    void setOldKey(const QString);
    void contentSetup();

public slots:
    void applyChanges()
    {
        emit JSONchanged();
    }
signals:
    void JSONchanged();

private slots:
    void on_buttonBoxEntry_clicked(QAbstractButton *button);

    void on_pushButtonEntryAdd_clicked();

    void on_listViewIngredients_clicked(const QModelIndex &index);

    void on_pushButtonSave_clicked();

    void on_pushButtonDelete_clicked();

private:
    Ui::EntryDialog *ui;
    std::string oldKey;
    QStringListModel* listViewModel;
};

#endif // ENTRYDIALOG_H
