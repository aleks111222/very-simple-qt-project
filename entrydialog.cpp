#include "entrydialog.h"
#include "ui_entrydialog.h"
#include "json.hpp"
#include <string>
#include "mainwindow.h"
#include <fstream>
#include <QDebug>
#include <iostream>
#include <sstream>

using json = nlohmann::json;

bool isFloat( std::string myString ) {
    std::istringstream iss(myString);
    float f;
    iss >> std::noskipws >> f; // noskipws considers leading whitespace invalid
    // Check the entire string was consumed and if either failbit or badbit is set
    return iss.eof() && !iss.fail();
}

void EntryDialog::contentSetup()
{
    std::ifstream file("recipes.json");
    json jf = json::parse(file);

    std::string description;
    std::string ingredient;

    QStringList ingredientsList;

    for (unsigned int i = 0; i < jf[oldKey]["recipe"].size(); i++)
    {
        std::string part = jf[oldKey]["recipe"][i];
        description.append(part + "\n");
    }

    for (json::iterator it = jf[oldKey].begin(); it != jf[oldKey].end(); ++it) {
        std::string str1 = it.key();

        if (str1 == "recipe")
            continue;

        std::string str2 = it.value();
        ingredient = (str1 + " " + str2);

        if(ingredient != "")
            ingredientsList << QString::fromStdString(ingredient);
    }

    QString qName = QString::fromStdString(oldKey);
    QString qDescription = QString::fromStdString(description);

    ui->lineEditRecipeName->setText(qName);
    ui->textEditRecipe->setText(qDescription);
    listViewModel->setStringList(ingredientsList);
    ui->listViewIngredients->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listViewIngredients->setModel(listViewModel);
}

void EntryDialog::updateJSON(json& newData)
{
    std::ifstream file("recipes.json");
    json jf = json::parse(file);

    for (json::iterator it = jf.begin(); it != jf.end(); ++it)
    {
        std::string name = it.key();

        if (name == oldKey && jf.size() != 1)
        {
            jf.erase(it++);
        } else if (name == oldKey) {
            jf.erase(it);
        }
    }

    jf.insert(newData.begin(), newData.end());

    file.close();

    std::ofstream File("recipes.json");

    File << jf;

    File.close();

    applyChanges();
}

EntryDialog::EntryDialog(QString oldKey, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EntryDialog)
{
    ui->setupUi(this);
    listViewModel = new QStringListModel(this);
    this->oldKey = oldKey.toStdString();
    contentSetup();
}

EntryDialog::EntryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EntryDialog)
{
    ui->setupUi(this);
    listViewModel = new QStringListModel(this);
}

EntryDialog::~EntryDialog()
{
    delete ui;
}

void EntryDialog::on_buttonBoxEntry_clicked(QAbstractButton *button)
{
    if((QAbstractButton *)button == ui->buttonBoxEntry->button(QDialogButtonBox::Ok) ||
       (QAbstractButton *)button == ui->buttonBoxEntry->button(QDialogButtonBox::Apply) ){

        QString qNewRecipeName = ui->lineEditRecipeName->text();
        QStringList qNewRecipeDescription = ui->textEditRecipe->toPlainText().split('\n');

        QAbstractItemModel* model = listViewModel;
        QStringList qNewIngredients;

        for ( int i = 0 ; i < model->rowCount() ; ++i )
        {
            qNewIngredients << model->index( i, 0 ).data( Qt::DisplayRole ).toString() ;
        }

        std::string newRecipeName = qNewRecipeName.toStdString();

        json newData =
        {
            { newRecipeName, { {"recipe", {} } } }
        };

        qNewIngredients.removeAll("");
        qNewRecipeDescription.removeAll("");

        for (int i = 0; i < qNewRecipeDescription.size(); i++)
        {
            newData[newRecipeName]["recipe"].push_back(qNewRecipeDescription[i].toStdString());
        }

        for (int i = 0; i < qNewIngredients.size(); i++)
            {
                std::string full = qNewIngredients[i].toStdString();

                std::stringstream ss;

                ss << full;

                std::string temp;
                float found;
                while (!ss.eof()) {

                    ss >> temp;

                    if (std::stringstream(temp) >> found)
                    break;

                    temp = "";
                }
                ss.str("");
                ss << found;

                std::string name = full.substr(0, full.find(ss.str()) - 1);

                if (name == "recipe")
                    continue;

                std::string value = full.substr(full.find(ss.str()), full.length() - 1);

                json addInfo = { {name, value} };
                newData[newRecipeName].insert(addInfo.begin(), addInfo.end());
            }

        updateJSON(newData);
    }

    return;
}

void EntryDialog::on_pushButtonEntryAdd_clicked()
{
    if (ui->lineEditItemName->text() == "" || ui->lineEditItemQuantity->text() == "" || ui->lineEditItemUnit->text() == ""
            || !isFloat(ui->lineEditItemQuantity->text().toStdString()))
        return;

    QString qIngredient;
    qIngredient.append(ui->lineEditItemName->text() + " " + ui->lineEditItemQuantity->text() + " " + ui->lineEditItemUnit->text());

    QAbstractItemModel* model = listViewModel;
    QStringList qNewIngredients;

    for ( int i = 0 ; i < model->rowCount() ; ++i )
    {
        std::string full = model->index( i, 0 ).data( Qt::DisplayRole ).toString().toStdString();

        std::stringstream ss;

        ss << full;

        std::string temp;
        float found;
        while (!ss.eof()) {

            ss >> temp;

            if (std::stringstream(temp) >> found)
                break;

            temp = "";
        }
        ss.str("");
        ss << found;

        std::string name = full.substr(0, full.find(ss.str()) - 1);
        std::string value = full.substr(full.find(ss.str()), full.length() - 1);
        std::string unit = value.substr(value.find(" ") + 1, full.length() - 1);

        if (name != ui->lineEditItemName->text().toStdString() || ui->lineEditItemUnit->text().toStdString() != unit)
            qNewIngredients << model->index( i, 0 ).data( Qt::DisplayRole ).toString() ;
    }

    qNewIngredients << qIngredient;

    listViewModel->setStringList(qNewIngredients);
    ui->listViewIngredients->setModel(listViewModel);
}

void EntryDialog::on_listViewIngredients_clicked(const QModelIndex &index)
{
    QString itemSelected = index.data(Qt::DisplayRole).toString();

    std::string full = itemSelected.toStdString();

    std::stringstream ss;

        ss << full;

        std::string temp;
        float found;
        while (!ss.eof()) {

            ss >> temp;

            if (std::stringstream(temp) >> found)
                break;

            temp = "";
        }
        ss.str("");
        ss << found;

        std::string value = full.substr(full.find(ss.str()), full.length() - 1);
        std::string name = full.substr(0, full.find(ss.str()) - 1);
        std::string unit = value.substr(value.find(" ") + 1, full.length() - 1);
        std::string quantity = ss.str();

    ui->lineEditItemName->setText(QString::fromStdString(name));
    ui->lineEditItemQuantity->setText(QString::fromStdString(quantity));
    ui->lineEditItemUnit->setText(QString::fromStdString(unit));
}

void EntryDialog::on_pushButtonSave_clicked()
{
    if (ui->lineEditItemName->text() != "" && ui->lineEditItemQuantity->text() != "" && ui->lineEditItemUnit->text() != ""
            && isFloat(ui->lineEditItemQuantity->text().toStdString())) {

        QAbstractItemModel* model = listViewModel;
        QStringList qChangedIngredients;

        for ( int i = 0 ; i < model->rowCount() ; ++i )
        {
            std::string full = model->index( i, 0 ).data( Qt::DisplayRole ).toString().toStdString();

            std::stringstream ss;

            ss << full;

            std::string temp;
            float found;
            while (!ss.eof()) {

                ss >> temp;

                if (std::stringstream(temp) >> found)
                    break;

                temp = "";
            }
            ss.str("");
            ss << found;

            std::string name = full.substr(0, full.find(ss.str()) - 1);
            std::string value = full.substr(full.find(ss.str()), full.length() - 1);
            std::string unit = value.substr(value.find(" ") + 1, full.length() - 1);

            QModelIndex index = ui->listViewIngredients->currentIndex();
            QString selectedIngredient = index.data(Qt::DisplayRole).toString();

            if ((name != ui->lineEditItemName->text().toStdString() || ui->lineEditItemUnit->text().toStdString() != unit) && QString::fromStdString(full) != selectedIngredient)
                qChangedIngredients << model->index( i, 0 ).data( Qt::DisplayRole ).toString();
        }


        QString qIngredient;
        qIngredient.append(ui->lineEditItemName->text() + " " + ui->lineEditItemQuantity->text() + " " + ui->lineEditItemUnit->text());

        qChangedIngredients << qIngredient;

        listViewModel->setStringList(qChangedIngredients);
        ui->listViewIngredients->setModel(listViewModel);
    }
}

void EntryDialog::on_pushButtonDelete_clicked()
{
    if (ui->lineEditItemName->text() == "" || ui->lineEditItemQuantity->text() == "" || ui->lineEditItemUnit->text() == "")
        return;

    QAbstractItemModel* model = listViewModel;
    QStringList qChangedIngredients;

    for ( int i = 0 ; i < model->rowCount() ; ++i )
    {
        std::string full = model->index( i, 0 ).data( Qt::DisplayRole ).toString().toStdString();

        std::stringstream ss;

            ss << full;

            std::string temp;
            float found;
            while (!ss.eof()) {

                ss >> temp;

                if (std::stringstream(temp) >> found)
                    break;

                temp = "";
            }
            ss.str("");
            ss << found;

            std::string name = full.substr(0, full.find(ss.str()) - 1);
            std::string value = full.substr(full.find(ss.str()), full.length() - 1);
            std::string unit = value.substr(value.find(" ") + 1, full.length() - 1);

            if (name != ui->lineEditItemName->text().toStdString() || ui->lineEditItemUnit->text().toStdString() != unit)
            qChangedIngredients << model->index( i, 0 ).data( Qt::DisplayRole ).toString() ;
    }

    listViewModel->setStringList(qChangedIngredients);
    ui->listViewIngredients->setModel(listViewModel);
}
